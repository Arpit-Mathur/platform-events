**Platform Events**
Platform events simplify the process of communicating changes and responding to events. 
Publishers and subscribers communicate with each other through events.
One or more subscribers can listen to the same event and carry out actions.

---
**Create Platform Event from Setup**

Define platform event similar like custom object, go to setup –> Integrations –> Platform events –> 
create new platform events just like a custom object and create new fields as required.
https://docs.google.com/document/d/1_244P9NKf_pxniAptubsAW7V1hLmCRVpRaMjK3HDh40/edit?usp=sharing


**Publish Using Apex**

A trigger processes platform event notification sequentially
in the order they’re received and trigger runs in its own process asynchronously 
and isn’t part of the transaction that published the event. Salesforce has a special class to 
publish the platform events EventBus which is having methods publish method. once the event is 
published you can consume the events from the channel

**Subscribe Using empApi in lwc**

Subscribe to platform events with the empApi component in your Lightning web component or Aura component. 
The empApi component provides access to methods for subscribing to a streaming channel and listening to event messages.
The empApi component uses a shared CometD-based Streaming API connection, enabling you to run multiple streaming apps in the 
browser for one user. The connection is not shared across user sessions.

