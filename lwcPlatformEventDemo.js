import {LightningElement} from 'lwc';
import {subscribe,unsubscribe,onError,setDebugFlag,isEmpEnabled} from 'lightning/empApi';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class LwcPlatformEventDemo extends LightningElement {
    channelName = '/event/Demo_Platform_Event__e';
    receivedMessage;
    accountName = '';
    subscription = {};
    connectedCallback() {
        this.handleSubscribe();
    }
   
    handleSubscribe() {
        const messageCallback = (response) => {
            this.handleResponse(response);
        }
         //Used to subscribe the published platform event
        subscribe(this.channelName, -1, messageCallback).then(response => {
            console.log('Subscription request sent to: ', JSON.stringify(response.channel));
            this.subscription = response;
        });
        this.registerErrorListener();
        
    }

    //Fetch the response received from the Platform Event
    handleResponse(response){
        this.accountName = response.data.payload.Name__c;
        this.receivedMessage = response.data.payload.Message__c;
        let toastData = response['data']['payload'];
        const toastEvent = new ShowToastEvent({
            title: toastData['Title__c'] ? toastData['Title__c'] : 'Default Title',
            message: toastData['Message__c'] ? toastData['Message__c'] : 'Default Message',
            variant: toastData['Variant__c'] ? toastData['Variant__c'] : 'Warning',
           
        });
        this.dispatchEvent(toastEvent);
    }
        
    disconnectedCallback() {
        unsubscribe(this.subscription, response => {
            console.log('Un-Subscribed from Platform Event Toast');
            console.log(response);
        });
    }

    registerErrorListener() {
        onError(error => {
            console.log('Received error from server: ', JSON.stringify(error));
        });
    }
}